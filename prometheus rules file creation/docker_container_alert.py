import paramiko
import os
ip=input(str("Enter Target IP "))
inst_name=input(str("Enter Target server name "))
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(ip, username='ruby')
stdin, stdout, stderr = ssh.exec_command('./python.sh')
opt=stdout.readlines()
opt = "".join(opt)
a=opt.split("\n")
import shutil
for i in range(len(a)-1):
  file = "/home/sathish/python/"+a[i]+".yml"
  shutil.copy2("/home/sathish/python/template.yml", file)
  fread = open(file, "rt")
  data = fread.read()
  data = data.replace('cont_name', a[i])
  data = data.replace('server', inst_name)
  fread.close()
  fwrite = open(file, "wt")
  fwrite.write(data)
  fwrite.close()
